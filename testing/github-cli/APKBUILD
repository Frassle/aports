# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Leo <thinkabit.ukim@gmail.com>
pkgname=github-cli
pkgver=1.2.0
pkgrel=0
pkgdesc="CLI for dealing with GitHub"
options="net chmod-clean" # Need to fetch modules and clean them up
url="https://cli.github.com"
arch="all !mips !mips64"
license="MIT"
depends="git"
makedepends="go"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/cli/cli/archive/v$pkgver/github-cli-$pkgver-$pkgver.tar.gz
	"
builddir="$srcdir/cli-$pkgver"

build() {
	CGO_CPPFLAGS="$CPPFLAGS" \
	CGO_CFLAGS="$CFLAGS" \
	CGO_CXXFLAGS="$CXXFLAGS" \
	CGO_LDFLAGS="$LDFLAGS" \
	\
	make GH_VERSION="v$pkgver" bin/gh manpages
	bin/gh completion -s bash | install -Dm0644 /dev/stdin \
		share/bash-completion/completions/gh
	bin/gh completion -s zsh | install -Dm0644 /dev/stdin \
		share/zsh/site-functions/_gh
	bin/gh completion -s fish | install -Dm0644 /dev/stdin \
		share/fish/completions/gh.fish
}

check() {
	go test -v ./...
}

package() {
	install -Dm755 bin/gh "$pkgdir"/usr/bin/gh

	mkdir -p "$pkgdir"/usr
	cp -r share "$pkgdir"/usr
}

sha512sums="d37b49fd73b9e2e223b7e30fb6257527c3c86e89521b5de3a35afb96d5fe4fa9b415f246e738dbdfd5ca8fcfb04d93defcaed35b507c18b56b710c8f47262634  github-cli-1.2.0-1.2.0.tar.gz"
