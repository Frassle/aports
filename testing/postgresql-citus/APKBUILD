# Contributor: Michael Pirogov <vbnet.ru@gmail.com>
# Maintainer: Michael Pirogov <vbnet.ru@gmail.com>
pkgname=postgresql-citus
_projname=citus
pkgver=9.4.2
pkgrel=0
pkgdesc="Scalable PostgreSQL for multi-tenant and real-time analytics workloads"
url="https://github.com/citusdata/citus"
arch="all"
license="AGPL-3.0-only"
makedepends="autoconf postgresql-dev icu-dev"
provides="pg_cron=$pkgver-r$pkgrel"
install="$pkgname.post-install"
source="https://github.com/citusdata/citus/archive/v$pkgver/$_projname-$pkgver.tar.gz"
builddir="$srcdir/$_projname-$pkgver"
options="!check" # tests requires running postgresql cluster

prepare() {
	default_prepare
	autoreconf -fi
}

build() {
	./configure \
		--prefix=/usr \
		--without-libcurl
	make
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="41de916b5267f2ce8ad6aa63be4fbd4f3b3fad516754009094b86d64a7bfb296b72724e6f699fe558550b8c679ab3bf7457e6323ce02e93b663e052ed128288b  citus-9.4.2.tar.gz"
