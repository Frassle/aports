# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-phone-components
pkgver=5.20.2
pkgrel=0
pkgdesc="Modules providing phone functionality for Plasma"
# armhf blocked by extra-cmake-modules
# s390x blocked by libksysguard
arch="all !armhf !s390x"
url="https://www.plasma-mobile.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="plasma-nano qt5-qtquickcontrols2 plasma-workspace dbus-x11 kactivities plasma-pa plasma-nm libqofono breeze-icons"
makedepends="extra-cmake-modules kpeople-dev qt5-qtdeclarative-dev kactivities-dev plasma-framework-dev kservice-dev kdeclarative-dev ki18n-dev kio-dev kcoreaddons-dev kconfig-dev kbookmarks-dev kwidgetsaddons-dev kcompletion-dev kitemviews-dev kjobwidgets-dev solid-dev kxmlgui-dev kconfigwidgets-dev kauth-dev kcodecs-dev kpackage-dev kwindowsystem-dev kdbusaddons-dev knotifications-dev kwayland-dev telepathy-qt-dev libphonenumber-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-phone-components-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="9865df21571d4b7acf4b03c5cad3afcaaf750dbc1a5ef3dae55f64401e48a98fbcc339633f44b0b2af7eee62dd95f596ab8b8f37cd1a302b4f800207f21ad020  plasma-phone-components-5.20.2.tar.xz"
