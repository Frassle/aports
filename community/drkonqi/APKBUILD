# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=drkonqi
pkgver=5.20.2
pkgrel=0
pkgdesc="Manages the power consumption settings of a Plasma Shell"
# armhf blocked by extra-cmake-modules
# s390x blocked by kconfigwidgets
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev ki18n-dev kcoreaddons-dev kservice-dev kconfigwidgets-dev kjobwidgets-dev kio-dev kcrash-dev kcompletion-dev kxmlrpcclient-dev kwidgetsaddons-dev kwallet-dev knotifications-dev kidletime-dev syntax-highlighting-dev"
source="https://download.kde.org/stable/plasma/$pkgver/drkonqi-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_TESTING=OFF # Broken
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="cee642eb26b244a5e3ed040d5295a13fce44f484fa359db67856c54fee213cc58e53a2dca5e331c935f40b1816212860778ff3ff63b7fcda510858fdb6dfe1f7  drkonqi-5.20.2.tar.xz"
