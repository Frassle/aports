# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=powerdevil
pkgver=5.20.2
pkgrel=0
pkgdesc="Manages the power consumption settings of a Plasma Shell"
# armhf blocked by extra-cmake-modules
# s390x blocked by libksysguard
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="upower"
depends_dev="qt5-qtbase-dev qt5-qtx11extras-dev kactivities-dev kauth-dev kidletime-dev kconfig-dev kdbusaddons-dev solid-dev ki18n-dev kglobalaccel-dev kio-dev knotifyconfig-dev kwayland-dev kcrash-dev knotifications-dev libkscreen-dev plasma-workspace-dev bluez-qt-dev networkmanager-qt-dev eudev-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev"
source="https://download.kde.org/stable/plasma/$pkgver/powerdevil-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="cd9e19a22b5ecea2ee48ce0d4ccdb6683d8004a630746bb95f62ad90ed3755b862ee7b39390f383ef521f7981f6a705ab39205528c19aeda14ff0a7163e57f89  powerdevil-5.20.2.tar.xz"
