# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=bear
pkgver=3.0.1
pkgrel=0
pkgdesc="Tool which generates a compilation database for clang tooling"
url="https://github.com/rizsotto/Bear"
# ppc64le armv7 armhf: Limited by grpc
# s390x: Test failure <https://github.com/rizsotto/Bear/issues/309>
arch="all !s390x !ppc64le !armv7 !armhf"
license="GPL-3.0-or-later"
makedepends="cmake grpc grpc-dev fmt-dev spdlog-dev
	nlohmann-json protobuf-dev gtest-dev c-ares-dev"
subpackages="$pkgname-doc"
source="https://github.com/rizsotto/Bear/archive/$pkgver/bear-$pkgver.tar.gz"
builddir="$srcdir/Bear-$pkgver"

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_INSTALL_LIBEXECDIR=libexec/bear \
		-DCMAKE_BUILD_TYPE=None .
	make -C build
}

check() {
	cd build
	ctest --verbose --output-on-failure
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

sha512sums="bba828057f4ed49b406320e58d30b47c2af9cf2c27ef2d89f9ecf9c35274ae89f4b3473f05f61a33ea30f86b099acdb624f24fe892c0b842311a89a779e0d422  bear-3.0.1.tar.gz"
