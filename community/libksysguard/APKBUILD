# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libksysguard
pkgver=5.20.2
pkgrel=0
pkgdesc="KDE system monitor library"
# armhf blocked by extra-cmake-modules
# mips, mips64, s390x blocked by kauth
arch="all !armhf !mips !mips64 !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="LGPL-2.1-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	kauth-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qttools-dev
	qt5-qtwebchannel-dev
	zlib-dev
	"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/plasma/$pkgver/libksysguard-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# processtest requires working OpenGL
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "processtest"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="54e1961c225ab5d5c1fa4b80df8f5b4d86821349eca75b5755e4cce094f938e2e81abbfb0272e7e90d9fe0a764618ae66eff15b030f3870f1ec490348fda4c5d  libksysguard-5.20.2.tar.xz"
