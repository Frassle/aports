# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-thunderbolt
pkgver=5.20.2
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# s390x blocked by bolt
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
pkgdesc="Plasma integration for controlling Thunderbolt devices"
license="GPL-2.0-only OR GPL-3.0-only"
depends="bolt"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev kcoreaddons-dev kcmutils-dev kdeclarative-dev ki18n-dev kdbusaddons-dev knotifications-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="8f0432d0c9eae7e95cd71355160b50f900251524a3599d59f8cdf8f62afefc4a1875f8cc68da598f33645eeda9d5ab6ed6110ac210593c8e4e5cad75ab979eb4  plasma-thunderbolt-5.20.2.tar.xz"
